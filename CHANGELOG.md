# Changelog : Bannière

Changelog du plugin SPIP **Bannière**.


## 1.0.1 - 2024-11-13

## Fixed

- Requette SQL pour le piepline `optimiser_base_disparus`


## 1.0.0 - 2024-09-16

## Added

- Documentation du plugin


## 0.3.1 - 2024-07-19

## Fixed

- Formulaire de la bannière du site


## 0.3.0 - 2024-07-19

## Added

- Héritage de la bannière du site


## 0.2.0 - 2024-07-18

## Added

- Formulaire de configuration des bannières
- Formulaire de téléversement d'une bannière
- Aperçu du cadrage de la bannière
- Pipeline SPIP `declarer_tables_banniere`
- Pipeline SPIP `libeller_banniere`
- Pipeline SPIP `quete_banniere_objet`
- Balise SPIP `#BANNIERE_SITE_SPIP`
- Balise SPIP `#BANNIERE_$OBJET`
- Balise SPIP `#BANNIERE_$OBJET_RUBRIQUE`
- Squelette `inclure/banniere.html`


## 0.1.0

- Version initiale du plugin