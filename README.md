# Plugin SPIP : Bannière

Le plugin ajoute la gestion d'une image de bannière pour vos objets éditoriaux. Il est basé sur le même fonctionnement que pour les logos SPIP.

Une fois le plugin activé, vous pouvez ajouter une bannière pour tous vos objets éditoriaux.
Une balise `#BANNIERE_XXXX` est disponible pour vos squelettes.

![Formulaire de configuration des bannières](demo/formulaire_configurer_bannieres.png)
![Formulaire de téléversement de la bannière du site](demo/formulaire_joindre_banniere_site_spip.png)
![Formulaire de téléversement de la bannière standard des rubriques](demo/formulaire_joindre_banniere_standard_des_rubriques.png)
![Formulaire de téléversement de la bannière d'une rubrique](demo/formulaire_joindre_banniere_rubrique.png)
![Formulaire de téléversement de la bannière d'un article](demo/formulaire_joindre_banniere_article.png)


## Compatibilité et prérequis du plugin SPIP

Le plugin est compatible avec :

 * [SPIP 4.2+](https://www.spip.net)
 * [PHP 8.1+](https://www.php.net)

Le plugin s'appuie sur les plugins suivant :

 * [`medias`](https://plugins.spip.net/medias.html) pour gérer les bannières comme des documents
 * [`bigup`](https://plugins.spip.net/bigup.html) pour le téléversement ajax des bannières
 * [`centre_image`](https://plugins.spip.net/centre_image.html) pour donner la main sur le recadrage automatique de la bannière


## Configurations

Une fois le plugin installé et activé les configurations suivantes sont disponibles :

### Espace privé

 * Contenu du site : Un nouveau formulaire permet de configurer les objets éditoriaux qui peuvent avoir une bannière
 * Identité du site : Comme pour le logo, un formulaire de téléversement est disponible pour la bannière du site

### Constantes PHP

À définir dans votre fichier `config/mes_options.php`

* `_BANNIERE_RATIO_PAR_DEFAUT` : ratio de cadrage automatique des bannières (par défaut : `32/9`)
* `_BANNIERE_RUBRIQUE_DESACTIVER_HERITAGE` : activez cette constante pour désactiver l'héritage des bannières de rubriques
* `_BANNIERE_SITE_DESACTIVER_HERITAGE` : activez cette constante pour désactiver l'héritage de la bannière du site

### Pipelines SPIP

 * `declarer_tables_banniere` : permet de déclarer (ou désactiver) la gestion des bannières pour un ou plusieurs objets SPIP
 * `libeller_banniere` : appelé par le formulaire de téléversement des bannières, il permet de libeller une bannière en fonction d'un objet SPIP
 * `quete_banniere_objet` : comme pour les logos, appelé par la fonction `quete_banniere_objet()` ce pipeline est à utiliser pour chercher une alternative quand il n'y a pas de bannière disponible (ou pour surcharger une bannière déjà téléversée)


## Nouvelles balises SPIP : `#BANNIERE_XXXX`

Les balises `#BANNIERE_XXXX` affichent les bannières des objets éditoriaux

 * `#BANNIERE_SITE_SPIP` : bannière du site
 * `#BANNIERE_ARTICLE` : bannière d'un article
 * `#BANNIERE_RUBRIQUE` : bannière d'une rubrique
 * `#BANNIERE_AUTEUR` : bannière d'un auteur
 * ... et plus généralement `#BANNIERE_NOM-OBJET-EDITORIAL`

### Héritage de la bannière du site

Par défaut, la balise `#BANNIERE_NOM-OBJET-EDITORIAL` affiche la bannière de l'objet demandé, et si elle n'existe pas, va automatiquement essayer d'afficher la bannière du site.

Pour désactiver cette fonction d'héritage, on peut définir la constante `_BANNIERE_SITE_DESACTIVER_HERITAGE`.

### Héritage des bannières de rubriques

Comme pour les logos SPIP, pour afficher une bannière avec un héritage des bannières de rubriques, la balise `#BANNIERE_NOM-OBJET-EDITORIAL_RUBRIQUE` affiche la bannière de l'objet, et si elle n'existe pas, va essayer d'afficher la bannière de la rubrique (en cherchant récursivement la bannière de la rubrique parente). Si aucune bannière de rubriques n'existe, alors la bannière du site sera affichée si elle existe.

Cet héritage est disponible pour tous les objets éditoriaux comme par exemple : `#BANNIERE_AUTEUR_RUBRIQUE` ou `#BANNIERE_MOT_RUBRIQUE`

Concernant l'objet `RUBRIQUE`, par défaut la balise `#BANNIERE_RUBRIQUE` affiche la bannière de la rubrique en cours et si elle n'existe pas, cherche récursivement la bannière de la rubrique parente, puis la bannière du site si elle existe.

Pour désactiver cette fonction d'héritage des bannières de rubriques, on peut définir la constante `_BANNIERE_RUBRIQUE_DESACTIVER_HERITAGE`.

### Syntaxe de la balise

Pour afficher la bannière d'un article : 

```
[(#PLUGIN{banniere}|oui)[
	#BANNIERE_ARTICLE
]
```

Pour afficher la bannière avec un lien sur l'image de la bannière : 

```
[(#PLUGIN{banniere}|oui)[
	#BANNIERE_ARTICLE{lien=#URL_ARTICLE}
]
```

Pour obtenir un tableau des valeurs de la bannière :

```
[(#PLUGIN{banniere}|oui)[
	#BANNIERE_ARTICLE*
]
```

Pour obtenir uniquement le chemin de l'image de la bannière :

```
[(#PLUGIN{banniere}|oui)[
	#BANNIERE_ARTICLE**
]
```


## Plugin `centre_image`

Avec le plugin `centre_image` activé, un aperçu du recadrage automatique de la bannière est affiché.
Il est possible de re-positionner ce cadrage en déplaçant la croix de l'aperçu.

Le plugin recadre automatiquement (au mieux) l'image en fonction du centre d'intérêt.

À noter que, si le ratio de l'image envoyée est plus petit que le ratio de configuration par défaut des bannières,
alors, seul le centre d'intérêt de l'image sera modifiable.

![Formulaire de téléversement de la bannière du site avec `centre_image`](demo/formulaire_joindre_banniere_site_spip_avec_centre_image.png)
![Formulaire de téléversement de la bannière d'un article avec `centre_image`](demo/formulaire_joindre_banniere_article_avec_centre_image.png)