<?php
/**
 * Gestion de l'action joindre_banniere
 *
 * @author		Jonathan OCHEJ
 * @license		GPL - https://www.gnu.org/licenses/gpl-3.0.html
 **/
if ( !defined('_ECRIRE_INC_VERSION') ) {
	return;
}

/**
 * Ajouter une bannière (au format $_FILES)
 *
 * @param string $objet
 *     Objet SPIP auquel sera lié la bannière (ex. article)
 * @param int $id_objet
 *     Identifiant de l'objet
 * @param array $file
 *     Propriétes au format $_FILE étendu :
 *     - string tmp_name : source sur le serveur
 *     - string name : nom du fichier envoye
 *     - bool titrer : donner ou non un titre a partir du nom du fichier
 * 
 * @return int|string
 *     - int : l'id_document ajoutée/modifiée (opération réussie)
 *     - string : une erreur s'est produit, la chaine est le message d'erreur
 */
function action_joindre_banniere_dist($objet, $id_objet, $file) {
	if ( empty($objet) ) {
		return _T('medias:erreur_objet_absent');
	}

	if ( $objet != 'site' ) {
		$objet = objet_type($objet);
	}

	// rechercher la bannière de l'objet
	$chercher_banniere = charger_fonction('chercher_banniere', 'inc');
	$banniere = $chercher_banniere($objet, $id_objet);
	$id_banniere = $banniere ? ($banniere[5]['id_document'] ?? 'new') : 'new';

	include_spip('inc/autoriser');
	autoriser_exception('associerdocuments', $objet, $id_objet);
	$ajouter_documents = charger_fonction('ajouter_documents', 'action');
	$ajouter_banniere = $ajouter_documents($id_banniere, [$file], $objet, $id_objet, 'banniere');
	autoriser_exception('associerdocuments', $objet, $id_objet, false);

	if ( defined('_TMP_ZIP') ) {
		unlink(_TMP_ZIP);
	}
	if ( defined('_TMP_DIR') ) {
		effacer_repertoire_temporaire(_TMP_DIR);
	}

	$id_document = reset($ajouter_banniere);
	if ( !is_numeric($id_document) ) {
		$erreur = $id_document ?: 'Erreur inconnue';
		spip_log("action_joindre_banniere_dist() - $erreur - file=".json_encode($file, JSON_THROW_ON_ERROR), 'banniere'._LOG_ERREUR);
		return $erreur;	
	}

	// invalider les caches de l'objet
	include_spip('inc/invalideur');
	suivre_invalideur("id='$objet/$id_objet'");

	return $id_document; // tout est bon, pas d'erreur
}