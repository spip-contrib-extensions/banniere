<?php
/**
 * Gestion de l'action
 *
 * @author		Jonathan OCHEJ
 * @license		GPL - https://www.gnu.org/licenses/gpl-3.0.html
 **/
if ( !defined('_ECRIRE_INC_VERSION') ) {
	return;
}

/**
 * Supprimer une bannière
 *
 * @param string $arg
 *     Fournit les arguments de la fonction sous la forme `$objet`_`$id_objet`
 */
function action_supprimer_banniere_dist($objet, $id_objet) {

	// appel direct depuis une url avec arg = "objet/id_objet"
	if ( is_null($objet) || is_null($id_objet) ) {
		$securiser_action = charger_fonction('securiser_action', 'inc');
		$arg = $securiser_action();
		[$objet, $id_objet] = array_pad(explode('/', (string) $arg, 2), 2, null);
	}

	// appel incorrect ou depuis une url erronnée interdit
	if ( is_null($objet) || is_null($id_objet) ) {
		return false;
	}

	$id_objet = intval($id_objet);

	// autorisé ?
	include_spip('inc/autoriser');
	if ( autoriser('joindrebanniere', $objet, $id_objet) ) {

		// rechercher la bannière de l'objet
		$chercher_banniere = charger_fonction('chercher_banniere', 'inc');
		$banniere = $chercher_banniere($objet, $id_objet);
		$id_banniere = $banniere ? ($banniere[5]['id_document'] ?? false) : false;

		if ( $id_banniere ) {
			autoriser_exception('supprimer', 'document', $id_banniere);
			$supprimer_document = charger_fonction('supprimer_document', 'action');
			$supprimer_banniere = $supprimer_document($id_banniere);
			autoriser_exception('supprimer', 'document', $id_banniere, false);

			if ( !$supprimer_banniere ) {
				spip_log("action_supprimer_banniere_dist() - Échec supprimer_document($id_banniere) - objet=$objet, id_objet=$id_objet, id_banniere=$id_banniere", 'banniere'._LOG_ERREUR);
			}

			// invalider les caches de l'objet
			include_spip('inc/invalideur');
			suivre_invalideur("id='$objet/$id_objet'");

			return $supprimer_banniere;
		}
	}
	else {
		spip_log("action_supprimer_banniere_dist() - Échec autoriser('joindrebanniere') - objet=$objet, id_objet=$id_objet", 'banniere'._LOG_ERREUR);
	}

	return false;
}