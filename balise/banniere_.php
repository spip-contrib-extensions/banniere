<?php
/**
 * Fonctions génériques pour les balises `#BANNIERE_XXXX`
 *
 * @author		Jonathan OCHEJ
 * @license		GPL - https://www.gnu.org/licenses/gpl-3.0.html
 **/
if ( !defined('_ECRIRE_INC_VERSION') ) {
	return;
}

/**
 * Compile la balise dynamique `#BANNIERE_XXXX` qui retourne le code HTML
 * pour afficher l'image de la bannière d'un objet éditorial de SPIP.
 *
 * Le type d'objet est récupéré dans le nom de la balise, tel que
 * `#BANNIERE_ARTICLE` ou `#BANNIERE_SITE`.
 *
 * Ces balises ont quelques options :
 *
 * - On peut demander une banniere de rubrique en absence de logo sur l'objet éditorial
 *   demandé avec `#BANNIERE_ARTICLE_RUBRIQUE`
 * - `#BANNIERE_XXXX*` retourne un tableau contenant toutes les informations de la bannière
 * - `#BANNIERE_XXXX**` retourne uniquement chemin du fichier de la bannière
 *
 * @param Spip\Compilateur\Noeud\Champ $p
 *     Pile au niveau de la balise
 * @return Spip\Compilateur\Noeud\Champ
 *     Pile complétée par le code à générer
 */
function balise_BANNIERE__dist($p) {

	preg_match(',^BANNIERE_([A-Z_]+?)(|_RUBRIQUE)$,i', $p->nom_champ, $regs);
	$type = strtolower($regs[1]);
	$suite_banniere = $regs[2];

	// pas de bannières distantes
	$connect = $p->id_boucle ? $p->boucles[$p->id_boucle]->sql_serveur : '';
	if ( $connect ) {
		spip_log("balise_BANNIERE__dist() - Les bannières distantes ne sont pas prévues ! - nom_champ=$p->nom_champ", 'banniere'._LOG_ERREUR);
		$p->code = "''";
		return $p;
	}

	// cas de #BANNIERE_SITE_SPIP
	if ( $type == 'site_spip' ) {
		$type = 'site';
		$_id_objet = "\"'0'\"";
	}

	// gestion du contexte
	$_contexte = argumenter_inclure($p->param, true, $p, $p->boucles, $p->id_boucle, false);
	// critere d'inclusion {env} (et {self} pour compatibilite ascendante)
	$flag_env = false;
	if ( isset($_contexte['env']) || isset($_contexte['self']) ) {
		$flag_env = true;
		unset($_contexte['env']);
	}
	// ne pas prendre en compte ajax
	if ( isset($_contexte['ajax']) ) {
		unset($_contexte['ajax']);
	}
	// ne pas prendre en compte la langue (inclue par l'appel du squelette)
	if ( isset($_contexte['lang']) ) {
		unset($_contexte['lang']);
	}
	$_contexte = 'array('.join(",\n\t", $_contexte).')';
	if ( $flag_env ) {
		$_contexte = "array_merge(\$Pile[0],$_contexte)";
	}

	$id_objet = id_table_objet($type);
	if ( !isset($_id_objet) ) {
		$_id_objet = champ_sql($id_objet, $p);
	}

	// rubrique ?
	$_id_rubrique = "''";
	if ( $type === 'rubrique' ) {
		$_id_rubrique = "quete_parent($_id_objet)";
	}
	else if ( $suite_banniere === '_RUBRIQUE' ) {
		$_id_rubrique = champ_sql('id_rubrique', $p);
	}

	$code = "quete_banniere('$id_objet', $_id_objet, $_id_rubrique, $_contexte)";

	// retourne le chemin du fichier de la bannière
	if ( $p->etoile === '**' ) {
		$code = "table_valeur($code,'chemin')";
	}
	// retourne un tableau des informations de la bannière
	else if ( $p->etoile === '*' ) {
		$code = $code; // ne rien faire, on a déja un tableau !
	}
	// retourne le code HTML de la bannière
	else {
		// ajoute le contexte de compilation pour faciliter le debug en cas d'erreur
		$code = "quete_banniere_html(".$code.", array(".memoriser_contexte_compil($p)."))";
	}

	$p->code = $code;

	// sécuritée assurée par le squelette
	$p->interdire_scripts = false;

	return $p;
}