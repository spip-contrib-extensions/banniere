<?php
/**
 * Fonctions du plugin chargées au calcul des squelettes
 *
 * @author		Jonathan OCHEJ
 * @license		GPL - https://www.gnu.org/licenses/gpl-3.0.html
 **/
if ( !defined('_ECRIRE_INC_VERSION') ) {
	return;
}

/**
 * Recenser les tables ou la bannière est déclarée
 * La bannière pourra alors être activée pour les objets de ces tables avec le formulaire de configuration des bannières
 *
 * @return array
 */
function lister_tables_banniere_declaree() {
	static $tables_banniere_declaree;

	if ( is_null($tables_banniere_declaree) ) {
		$tables_banniere_declaree = [];

		foreach ( lister_tables_objets_sql() as $table => $valeur ) {
			// pas de bannières de documents (évitons l'inception ^^)
			if ( $table != 'spip_documents' ) {
				// bannière déclarée avec le pipeline 'declarer_tables_objets_sql'
				if ( array_key_exists('banniere', $valeur) ) {
					if ( $valeur['banniere'] && $valeur['banniere'] != 'non' ) {
						$tables_banniere_declaree[] = $table;
					}
				}
				// par défaut, déclarer automatiquement tous les objets éditables
				else if ( $valeur['editable'] ) {
					$tables_banniere_declaree[] = $table;
				}
			}
		}
		
		// passer ce tableau dans un pipeline
		$tables_banniere_declaree = pipeline('declarer_tables_banniere', $tables_banniere_declaree);
	}

	return $tables_banniere_declaree;
}

/**
 * Recenser les tables ou la bannière est activée
 *
 * @return array
 */
function lister_tables_banniere_activee() {
	static $tables_banniere_activee;

	if ( is_null($tables_banniere_activee) ) {
		$banniere_tables_objets = $GLOBALS['meta']['banniere_tables_objets'] ?? '';

		$tables_banniere_activee = array_filter(explode(',', $banniere_tables_objets));

		// garder uniquement les tables déclarées
		$tables_banniere_activee = array_intersect($tables_banniere_activee, lister_tables_banniere_declaree());

		// pas de bannières de documents (évitons l'inception ^^)
		if ( ($cle = array_search('spip_documents', $tables_banniere_activee)) !== false ) {
			unset($tables_banniere_activee[$cle]);
		}

		// ré-index correctement le tableau si il y a eu des modifications
		$tables_banniere_activee = array_values($tables_banniere_activee);
	}

	return $tables_banniere_activee;
}

/**
 * Retourne la bannière d'un objet, éventuellement par héritage
 *
 * @param string $cle_objet
 *     Nom de la clé de l'objet dont on veut chercher la bannière
 * @param int $id
 *     Identifiant de l'objet dont on veut chercher la bannière
 * @param int $id_rubrique
 *     Identifiant de la rubrique parente si l'on veut aller chercher sa bannière
 *     dans le cas où l'objet demandé n'en a pas
 * @param array $env
 *     Tableau de clés/valeurs ajoutées aux valeurs de la bannière après la quête
 * @return array|false
 *     Tableau des valeurs de la bannière
 */
function quete_banniere($cle_objet, $id, $id_rubrique, $env = []) {
	include_spip('base/objets');

	$cle_objet = id_table_objet($cle_objet);

	while (1) {
		$objet = objet_type($cle_objet);

		$banniere = quete_banniere_objet($objet, $id);
		if ( $banniere ) {
			// ajoute le timestamp au chemin pour distinguer le changement de bannière
			$banniere['chemin'] .= $banniere['timestamp'] ? "?{$banniere['timestamp']}" : '';

			// ajoute les paramètres d'environnement
			if ( $env && is_array($env) ) {
				// supprimer les paramètres spécifiques au bon fonctionnement de la bannière pour ne pas les transmettre
				$env = array_diff_key($env, array_flip(['id','chemin','timestamp','extension','taille','largeur','hauteur']));
			}

			return array_merge($banniere,$env);
		}
		else {
			if ( defined('_BANNIERE_RUBRIQUE_DESACTIVER_HERITAGE') ) {
				// essayer d'afficher la bannière du site 
				if ( !defined('_BANNIERE_SITE_DESACTIVER_HERITAGE') && $cle_objet != 'site' ) {
					$cle_objet = 'site';
					$id = 0;
				}
				else {
					return false;
				}
			}
			else {
				if ( $id_rubrique ) {
					$cle_objet = 'id_rubrique';
					$id = $id_rubrique;
					$id_rubrique = 0;
				}
				else {
					if ( $id && $cle_objet == 'id_rubrique' ) {
						$id = quete_parent($id);
					}
					else {
						// essayer d'afficher la bannière du site
						if ( !defined('_BANNIERE_SITE_DESACTIVER_HERITAGE') && $cle_objet != 'site' ) {
							$cle_objet = 'site';
							$id = 0;
						}
						else {
							return false;
						}
					}
				}
			}
		}
	}
}

/**
 * Chercher une bannière d'un contenu précis
 *
 * @param string $objet
 * 		Type de l'objet
 * @param int $id_objet
 * 		Idenfiant de l'objet
 * @return array|false
 **/
function quete_banniere_objet($objet, $id_objet) {
	static $chercher_banniere;

	// cas sépcial pour la banière du site qui est toujours active
	if ( $objet != 'site' ) {
		$table_objet_sql = table_objet_sql($objet);
		// est-ce que la bannière est activée sur cet objet ?
		if ( !in_array($table_objet_sql, lister_tables_banniere_activee()) ) {
			spip_log("quete_banniere_objet() - Bannière inactive - objet=$objet, id_objet=$id_objet", 'banniere'._LOG_DEBUG);
			return false;
		}
	}

	if ( is_null($chercher_banniere) ) {
		$chercher_banniere = charger_fonction('chercher_banniere', 'inc');
	}

	$banniere = $chercher_banniere($objet, $id_objet);
	if ( !empty($banniere) ) {
		$infos = [
			'id'		=> ($banniere[5]['id_document'] ?? ''),
			'chemin' 	=> $banniere[0],
			'timestamp'	=> $banniere[4],
		];

		// valeurs en DB de la table `spip_documents`
		foreach ( ['extension', 'taille', 'largeur', 'hauteur', 'titre', 'descriptif', 'credits', 'alt'] as $champ ) {
			$infos[$champ] = ($banniere[5][$champ] ?? '');
		}
		// remplace les valeurs "normalisées"
		$banniere = $infos;
	}

	// garder une trace du chemin/timestamp pour savoir si il y a eu des changement avec le pipeline
	$chemin = $banniere ? $banniere['chemin'] : false;

	// passer cette recherche de bannière dans un pipeline
	$banniere = pipeline(
		'quete_banniere_objet',
		[
			'args' => [
				'objet' => $objet,
				'id_objet' => $id_objet,
				'cle_objet' => id_table_objet($objet),
			],
			'data' => $banniere,
		]
	);

	// vérification de la validitée de la bannière en complétant si besoin car on doit toujours renvoyer un tableau
	if ( !empty($banniere) ) {

		// le pipeline a renvoyé seulement un nouveau chemin
		if ( is_string($banniere) ) {
			$banniere = ['chemin' => $banniere];
		}

		if ( !is_array($banniere) || !array_key_exists('chemin', $banniere) ) {
			spip_log("quete_banniere_objet() - Bannière invalide - objet=$objet, id_objet=$id_objet", 'banniere'._LOG_ERREUR);
			return false;
		}

		// chemin invalide, est-ce un document dans le dossier des documents ?
		if ( !file_exists($banniere['chemin']) ) {
			if ( strncmp($banniere['chemin'], _DIR_IMG, strlen(_DIR_IMG)) !== 0) {
				$banniere['chemin'] = _DIR_IMG.$banniere['chemin'];
			}

			// deuxième essai
			if ( !file_exists($banniere['chemin']) ) {
				spip_log("quete_banniere_objet() - Chemin invalide - objet=$objet, id_objet=$id_objet, chemin{$banniere['chemin']}", 'banniere'._LOG_ERREUR);
				return false;
			}
		}

		// par sécurité, si le chemin a changé, on recalcul toujours timestamp/extension/taille/largeur/hauteur

		// timestamp
		if ( $banniere['chemin'] != $chemin || !array_key_exists('timestamp', $banniere) ) {
			$banniere['timestamp'] = @filemtime($banniere['chemin']);
		}
		// extension
		if ( $banniere['chemin'] != $chemin || !array_key_exists('extension', $banniere) ) {
			include_spip('inc/documents');
			$banniere['extension'] = corriger_extension(strtolower(pathinfo($banniere['chemin']['name'], PATHINFO_EXTENSION)));
		}
		// taille
		if ( $banniere['chemin'] != $chemin || !array_key_exists('taille', $banniere) ) {
			$banniere['taille'] = @intval(filesize($banniere['chemin']));
		}
		// largeur / hauteur
		if ( $banniere['chemin'] != $chemin || !array_key_exists('largeur', $banniere) || !array_key_exists('hauteur', $banniere) ) {
			$size = @spip_getimagesize($banniere['chemin']);
			$banniere['largeur'] = ($size ? $size[0] : '');
			$banniere['hauteur'] = ($size ? $size[1] : '');
		}

		return $banniere;
	}

	return false;
}

/**
 * Recuperer le HTML de la bannière d'apres ses informations
 * 
 * @param array $banniere
 * @param array $compil
 * 
 * @return string
 */
function quete_banniere_html($banniere, $compil = []) {
	if ( !is_array($banniere) ) {
		return '';
	}

	/* pour mémoire, mais géré directement par le squelette pour être indépendant du traitement des documents
	foreach ( ['titre', 'descriptif', 'credits', 'alt'] as $champ ) {
		if ( !empty($banniere[$champ]) ) {
			$banniere[$champ] = appliquer_traitement_champ($banniere[$champ] , $champ, 'document');
		}
	}
	*/

	// fond spécifique ?
	$fond = 'inclure/banniere';
	if ( array_key_exists('fond', $banniere) ) {
		$fond = $banniere['fond'];
		unset($banniere['fond']);
	}

	// ajout des options
	$options = ['trim' => true];
	// option `compil` si fourni pour faciliter le débug en cas d'erreur (voir `reconstruire_contexte_compil()`)
	if ( $compil && is_array($compil) ) {
		$options['compil'] = $compil;
	}

	return recuperer_fond($fond, $banniere, $options);
}