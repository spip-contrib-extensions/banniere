<?php
/**
 * Pipelines SPIP utilisés par le plugin
 *
 * @author		Jonathan OCHEJ
 * @license		GPL - https://www.gnu.org/licenses/gpl-3.0.html
 **/
if ( !defined('_ECRIRE_INC_VERSION') ) {
	return;
}

/**
 * @pipeline affiche_gauche
 *
 * @param array $flux
 * 
 * @return array
 */
function banniere_affiche_gauche($flux) {

	$exec = isset($flux['args']['exec']) ? $flux['args']['exec'] : '';

	$objet = null;
	$id_objet = null;

	// cas spécial pour la bannière standard des rubriques
	if ( $exec == 'rubriques' ) {
		$objet = 'rubrique';
		$id_objet = 0;
	}
	else if ( $objet_trouve = trouver_objet_exec($exec) ) {
		// ne pas afficher sur un objet en cours d'édition
		if ( $objet_trouve['edition'] === false ) {
			$objet = $objet_trouve['type'];
			$id_table_objet = $objet_trouve['id_table_objet'];
			$id_objet = isset($flux['args'][$id_table_objet]) ? intval($flux['args'][$id_table_objet]) : null;
		}
	}

	// objet valide et il y a un id (valide même si c'est `0`)
	if ( !empty($objet) && isset($id_objet) ) {

		$table_objet_sql = table_objet_sql($objet);
		include_spip('banniere_fonctions');

		// est-ce que la bannière est activée sur cet objet ? Si oui, ajoute la bannière (formulaire ou aperçu)
		if ( in_array($table_objet_sql, lister_tables_banniere_activee()) ) {
			$joindre_banniere = recuperer_fond('prive/squelettes/inclure/joindre-banniere', [
				'objet'		=> $objet,
				'id_objet'	=> $id_objet,
				'options'	=> [
					'reduire_apercu'	=> 250,
				]
			]);
	
			// essaye d'ajouter la bannière juste après la première `boite_infos` de l'objet (généré avec `#BOITE_OUVRIR{'','info'}` + `#BOITE_FERMER`)
			if (
				strpos($flux['data'], '<div class="box info">') !== false &&
				preg_match('/<div\s+class="box info"\s*>((?:(?:(?!<div[^>]*>|<\/div>).)++|<div[^>]*>(?1)<\/div>)*)<\/div>/UimsS', $flux['data'], $box_info_trouvee)
			) {
				$box_info_position = strpos($flux['data'], $box_info_trouvee[0]);
				$box_info_longueur = strlen($box_info_trouvee[0]);
	
				$joindre_banniere_position = $box_info_position + $box_info_longueur;
	
				$flux['data'] = substr_replace($flux['data'], $joindre_banniere, $joindre_banniere_position, 0);
			}
			// sinon, ajoute le contenu au début du flux (une bannière c'est le plus important non !?)
			else {
				$flux['data'] = $joindre_banniere.$flux['data'];
			}
		}
	}

	return $flux;
}

/**
 * @pipeline affiche_milieu
 *
 * @param array $flux
 * 
 * @return array
 */
function banniere_affiche_milieu($flux) {
	// ajoute la configuration des bannières à la page de configuration : Contenu du site
	if ( $flux['args']['exec'] == 'configurer_contenu' ) {
		$flux['data'] .= recuperer_fond('prive/squelettes/inclure/configurer', [
			'configurer' => 'configurer_bannieres'
		]);
	}
	// ajoute le formulaire de la bannière du site à la page de configuration : Identité du site
	else if ( $flux['args']['exec'] == 'configurer_identite' ) {
		$flux['data'] .= recuperer_fond('prive/squelettes/inclure/joindre-banniere', [
			'objet'		=> 'site',
			'id_objet'	=> 0,
			'options'	=> [
				'reduire_apercu'	=> 750,
			]
		]);
	}

	return $flux;
}

/**
 * @pipeline declarer_tables_objets_sql
 *
 * @param array $flux
 * 
 * @return array
 */
function banniere_declarer_tables_objets_sql($flux) {

	// ajoute le libellé de la bannière en fonction de certains objets éditoriaux
	// si le libellé n'existe pas, alors le mot "Bannière" sera utilisé par défaut
	if ( array_key_exists('spip_articles', $flux) ) {
		$flux['spip_articles']['texte_banniere_objet'] = 'banniere:banniere_article';
	}
	if ( array_key_exists('spip_auteurs', $flux) ) {
		$flux['spip_auteurs']['texte_banniere_objet'] = 'banniere:banniere_auteur';
	}
	if ( array_key_exists('spip_groupes_mots', $flux) ) {
		$flux['spip_groupes_mots']['texte_banniere_objet'] = 'banniere:banniere_groupe';
	}
	if ( array_key_exists('spip_mots', $flux) ) {
		$flux['spip_mots']['texte_banniere_objet'] = 'banniere:banniere_mot_cle';
	}
	if ( array_key_exists('spip_rubriques', $flux) ) {
		$flux['spip_rubriques']['texte_banniere_objet'] = 'banniere:banniere_rubrique';
	}

	return $flux;
}

/**
 * @pipeline header_prive
 *
 * @param string $flux
 * 
 * @return string
 */
function banniere_header_prive($flux) {

	$flux .="\n\t".'<script type="text/javascript" src="'.timestamp(find_in_path('javascript/banniere_edit.js')).'"></script>'."\n";

	if ( test_plugin_actif('centre_image') ) {
		$flux .="\n\t".'<script type="text/javascript" src="'.timestamp(find_in_path('javascript/centre_image_banniere.js')).'"></script>'."\n";
	}

	return $flux;
}

/**
 * @pipeline insert_head_css
 *
 * @param string $flux
 * 
 * @return string
 */
function banniere_insert_head_css($flux) {

	// css par défaut pour la bannière sur l'espace public
	$flux .= "\n\t".'<link rel="stylesheet" href="'.produire_fond_statique('css/banniere.css').'">'."\n";

	return $flux;
}

/**
 * @pipeline optimiser_base_disparus
 *
 * @param array $flux
 * 
 * @return array
 */
function banniere_optimiser_base_disparus($flux) {
	include_spip('inc/autoriser');

	// utilise l'action de suppression d'un document car cette bannière n'est plus liée à aucun objet
	$supprimer_document = charger_fonction('supprimer_document', 'action');

	// sélectionner les bannières qui ne sont plus liées à rien
	$r = sql_select(
		'D.id_document',
		'spip_documents AS D
			LEFT JOIN spip_documents_liens AS L
				ON (L.id_document=D.id_document)',
		'D.mode=\'banniere\' AND L.id_document IS NULL'
	);

	while ( $e = sql_fetch($r) ) {
		$id_banniere = $e['id_document'];

		// supprimer la bannière
		autoriser_exception('supprimer', 'document', $id_banniere);
		$supprimer_banniere = $supprimer_document($id_banniere);
		autoriser_exception('supprimer', 'document', $id_banniere, false);

		if ( $supprimer_banniere ) {
			spip_log("banniere_optimiser_base_disparus() - Bannière supprimée - id_banniere=$id_banniere", 'banniere'._LOG_AVERTISSEMENT);
			$flux['data']++;
		}
	}

	return $flux;
}

/**
 * @pipeline pre_edition
 *
 * @param array $flux
 * 
 * @return array
 */
function banniere_pre_edition($flux) {

	// lors de l'action `instituer` d'un document
	// s'il sagit d'une bannière, ne jamais instituer pour ne pas perturber le fonctionnement de la publication des objets éditoriaux (notamment les rubriques)
	if (
		$flux['args']['table'] == 'spip_documents' AND
		$flux['args']['action'] == 'instituer' AND
		sql_fetsel('1', 'spip_documents', "mode='banniere' AND id_document=".intval($flux['args']['id_objet']))
	) {
		$flux['data'] = false;
	}

	return $flux;
}

/**
 * @pipeline recuperer_fond
 *
 * @param array $flux
 * 
 * @return array
 */
function banniere_recuperer_fond($flux) {

	// ajoute les traductions pour Bigup
	if ( $flux['args']['fond'] == 'javascript/bigup.trads.js' ) {
		$deposer_la_banniere_ici = texte_script(_T('banniere:deposer_la_banniere_ici'));
		$la_banniere_a_ete_envoyee = texte_script(_T('banniere:la_banniere_a_ete_envoyee'));

		$flux['data']['texte'] .= <<<HEREDOC_JAVASCRIPT
			Trads.set('banniere', {
				// D
				deposer_la_banniere_ici: "$deposer_la_banniere_ici",
				// L
				la_banniere_a_ete_envoyee: "$la_banniere_a_ete_envoyee",
			});
		HEREDOC_JAVASCRIPT;
	}

	return $flux;
}