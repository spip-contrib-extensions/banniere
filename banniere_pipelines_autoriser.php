<?php
/**
 * Fonctions d'autorisations du plugin SPIP
 *
 * @author		Jonathan OCHEJ
 * @license		GPL - https://www.gnu.org/licenses/gpl-3.0.html
 **/
if ( !defined('_ECRIRE_INC_VERSION') ) {
	return;
}

/**
 * Fonction d'appel pour le pipeline
 * 
 * @pipeline autoriser
 **/
function banniere_autoriser() { }

/**
 * Autorisation de joindre une bannière à un objet
 *
 * Il faut pouvoir modifier l'objet
 *
 * @see autoriser()
 *
 * @param string			$faire	Action demandée
 * @param string			$type	Type d'objet ou élément
 * @param int|string|null	$id		Identifiant
 * @param array				$qui	Description de l'auteur demandant l'autorisation
 * @param array				$opt	Tableau d'options sous forme de tableau associatif
 * 
 * @return bool	`true` si l'auteur est autorisée à exécuter l'action, sinon `false`
 **/
function autoriser_joindrebanniere_dist(string $faire, string $type, $id, array $qui, array $opt): bool {
	// par defaut, on a le droit de joindre une bannière si on a le droit de modifier l'objet
	return autoriser('modifier', $type, $id, $qui, $opt);
}

/**
 * Autorisation de joindre une bannière à une rubrique
 *
 * Il faut pouvoir publier dans la rubrique
 *
 * @see autoriser()
 *
 * @param string			$faire	Action demandée
 * @param string			$type	Type d'objet ou élément
 * @param int|string|null	$id		Identifiant
 * @param array				$qui	Description de l'auteur demandant l'autorisation
 * @param array				$opt	Tableau d'options sous forme de tableau associatif
 * 
 * @return bool	`true` si l'auteur est autorisée à exécuter l'action, sinon `false`
 **/
function autoriser_rubrique_joindrebanniere_dist(string $faire, string $type, $id, array $qui, array $opt): bool {
	return autoriser('publierdans', 'rubrique', $id, $qui, $opt);
}

/**
 * Autorisation de joindre une bannière à un auteur
 *
 * Il faut être administrateur complet ou que l'auteur soit celui qui demande l'autorisation
 *
 * @see autoriser()
 *
 * @param string			$faire	Action demandée
 * @param string			$type	Type d'objet ou élément
 * @param int|string|null	$id		Identifiant
 * @param array				$qui	Description de l'auteur demandant l'autorisation
 * @param array				$opt	Tableau d'options sous forme de tableau associatif
 * 
 * @return bool	`true` si l'auteur est autorisée à exécuter l'action, sinon `false`
 **/
function autoriser_auteur_joindrebanniere_dist(string $faire, string $type, $id, array $qui, array $opt): bool {
	$id = intval($id);
	return ( ($id == $qui['id_auteur']) || ( ($qui['statut'] === '0minirezo') && !$qui['restreint'] ) );
}

/**
 * Autorisation de joindre une bannière à un mot
 *
 * Il faut être administrateur complet
 *
 * @see autoriser()
 *
 * @param string			$faire	Action demandée
 * @param string			$type	Type d'objet ou élément
 * @param int|string|null	$id		Identifiant
 * @param array				$qui	Description de l'auteur demandant l'autorisation
 * @param array				$opt	Tableau d'options sous forme de tableau associatif
 * 
 * @return bool	`true` si l'auteur est autorisée à exécuter l'action, sinon `false`
 **/
function autoriser_mot_joindrebanniere_dist($faire, $type, $id, $qui, $opt) {
	return ( ($qui['statut'] == '0minirezo') && !$qui['restreint'] );
}

/**
 * Autorisation de joindre une bannière à un groupe de mot
 *
 * Il faut être administrateur complet
 *
 * @see autoriser()
 *
 * @param string			$faire	Action demandée
 * @param string			$type	Type d'objet ou élément
 * @param int|string|null	$id		Identifiant
 * @param array				$qui	Description de l'auteur demandant l'autorisation
 * @param array				$opt	Tableau d'options sous forme de tableau associatif
 * 
 * @return bool	`true` si l'auteur est autorisée à exécuter l'action, sinon `false`
 */
function autoriser_groupemots_joindrebanniere_dist($faire, $type, $id, $qui, $opt) {
	return ( ($qui['statut'] == '0minirezo') && !$qui['restreint'] );
}