<?php
/**
 * Gestion du formulaire de configurations des bannières
 *
 * @author		Jonathan OCHEJ
 * @license		GPL - https://www.gnu.org/licenses/gpl-3.0.html
 **/
if ( !defined('_ECRIRE_INC_VERSION') ) {
	return;
}

/**
 * Chargement du formulaire
 * 
 * @return array
 */
function formulaires_configurer_bannieres_charger_dist() {
	$valeurs = [];

	$valeurs['banniere_tables_objets'] = lister_tables_banniere_activee();

	$tables_banniere_declaree = lister_tables_banniere_declaree();

	$valeurs['banniere_tables_objets_declares'] = [];
	// on a besoin de récupérer la traduction du texte des objets pour l'afficher !
	foreach ( lister_tables_objets_sql() as $table => $valeur ) {
		if ( in_array($table, $tables_banniere_declaree) ) {
			$valeurs['banniere_tables_objets_declares'][$table] = _T(table_valeur($valeur, 'texte_objets'));
		}
	}

	return $valeurs;
}

/**
 * Traitement du formulaire
 * 
 * @return array $retours
 */
function formulaires_configurer_bannieres_traiter_dist() {
	$retours = [];

	// meta concernée
	$meta = 'banniere_tables_objets';

	// tableau de la configuration
	$tables = _request($meta);
	$tables = is_array($tables) ? array_filter($tables) : [];

	// valeur d'enregistrement
	$valeur = implode(',', $tables);

	if ( !empty($valeur) ) {
		ecrire_meta($meta, $valeur);
	}
	else {
		effacer_meta($meta);
	}

	// invalider tous les caches
	include_spip('inc/invalideur');
	suivre_invalideur('1');

	$retours['message_ok'] = _T('config_info_enregistree');

	return $retours;
}