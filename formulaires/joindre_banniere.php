<?php
/**
 * Gestion du formulaire de téléversement de bannière
 *
 * @author		Jonathan OCHEJ
 * @license		GPL - https://www.gnu.org/licenses/gpl-3.0.html
 **/
if ( !defined('_ECRIRE_INC_VERSION') ) {
	return;
}

// utilise pour la bannière du site, donc doit rester ici
$GLOBALS['banniere_libelles']['site'] = _T('banniere:banniere_site');
$GLOBALS['banniere_libelles']['racine'] = _T('banniere:banniere_standard_rubrique');

/**
 * Chargement du formulaire
 *
 * @param string $objet
 *     Objet SPIP auquel sera lié la bannière (ex. article)
 * @param int $id_objet
 *     Identifiant de l'objet
 * @param array $options
 *     Tableau d'options [
 *          'titre'				=> 'texte', // string - titre du formulaire
 *          'reduire_apercu'	=> 'largeur', // redimensionner l'aperçu de la bannière (par défaut `500`)
 *     ]
 * 
 * @return array|false
 */
function formulaires_joindre_banniere_charger_dist(string $objet, int $id_objet = 0, ?array $options = []) {

	if ( empty($objet) ) {
		return false;
	}

	// attention, la bannière du site est toujours active
	if ( $objet != 'site' || $id_objet !== 0 ) {
		// est-ce que la bannière est activée sur cet objet ?
		if ( !in_array(table_objet_sql($objet), lister_tables_banniere_activee()) ) {
			return false;
		}
	}

	// options facultatives
	$options = spip_sanitize_from_request(( !is_array($options) ? unserialize($options) : $options), '*');

	// valeurs d'environnement transmises au squelette du formulaire
	$valeurs = [];

	// titre du formulaire
	if ( empty($options['titre']) || !is_string($options['titre']) ) {
		$libelles = pipeline('libeller_banniere', $GLOBALS['banniere_libelles']);
		$libelle = ($id_objet || $objet != 'rubrique') ? $objet : 'racine';
		if ( isset($libelles[$libelle]) ) {
			$libelle = $libelles[$libelle];
		}
		else if ( $libelle = objet_info($objet, 'texte_banniere_objet') ) {
			$libelle = _T($libelle);
		}
		else {
			$libelle = _T('banniere:banniere');
		}
		$valeurs['titre'] = $libelle;
	}
	else {
		$valeurs['titre'] = $options['titre'];
		unset($options['titre']);
	}

	$valeurs['options'] = $options;

	$valeurs['editable'] = '';
	include_spip('inc/autoriser');
	if ( autoriser('joindrebanniere', $objet, $id_objet) ) {
		$valeurs['editable'] = ' ';
		if ( test_plugin_actif('bigup') ) {
			$valeurs['_bigup_rechercher_fichiers'] = true;
		}
	}

	$valeurs['objet'] = $objet;
	$valeurs['id_objet'] = $id_objet;

	// formats des bannières
	$formats_bannieres = charger_fonction('formats_bannieres', 'inc');
	$valeurs['formats_bannieres'] = $formats_bannieres();

	// rechercher la bannière de l'objet
	$chercher_banniere = charger_fonction('chercher_banniere', 'inc');
	$banniere = $chercher_banniere($objet, $id_objet);
	if ( $banniere ) {
		$valeurs['banniere'] = $banniere[0];
		$valeurs['id_banniere'] = $banniere[5]['id_document'] ?? '';
	}

	return $valeurs;
}

/**
 * Identifier le formulaire en faisant abstraction des parametres qui
 * ne representent pas l'objet édité
 *
 * @param string $objet
 *     Objet SPIP auquel sera lié la bannière (ex. article)
 * @param int|string $id_objet
 *     Identifiant de l'objet
 * 
 * @return string
 *     Hash du formulaire
 */
function formulaires_joindre_banniere_identifier_dist(string $objet, $id_objet) {
	return serialize([$objet, (int) $id_objet]);
}

/**
 * Vérification du formulaire
 *
 * @param string $objet
 *     Objet SPIP auquel sera lié la bannière (ex. article)
 * @param int $id_objet
 *     Identifiant de l'objet
 * 
 * @return array $erreurs
 */
function formulaires_joindre_banniere_verifier_dist(string $objet, int $id_objet) {
	$erreurs = [];

	if ( _request('supprimer') ) {
		// pas de vérification
	}
	else {
		$fichier = formulaires_joindre_banniere_trouver_fichier_envoye();
		if ( is_string($fichier) ) {
			$erreurs['message_erreur'] = $fichier;
		}
	
		if ( !empty($erreurs['message_erreur']) && defined('_TMP_DIR') ) {
			effacer_repertoire_temporaire(_TMP_DIR);
		}
	}

	return $erreurs;
}

/**
 * Traitement du formulaire
 *
 * @param string $objet
 *     Objet SPIP auquel sera lié la bannière (ex. article)
 * @param int $id_objet
 *     Identifiant de l'objet
 * 
 * @return array $retours
 */
function formulaires_joindre_banniere_traiter_dist(string $objet, int $id_objet) {
	$retours = ['editable' => true];

	if ( _request('supprimer') ) {
		$supprimer_banniere = charger_fonction('supprimer_banniere', 'action');
		$supprimer_banniere($objet,$id_objet);
	}
	else {
		$joindre_banniere = charger_fonction('joindre_banniere', 'action');

		$fichier = formulaires_joindre_banniere_trouver_fichier_envoye();
		$nouvelle_banniere = $joindre_banniere($objet, $id_objet, $fichier);

		if ( !is_numeric($nouvelle_banniere) ) {
			$retours['message_erreur'] = $nouvelle_banniere;
		}
	}

	return $retours;
}

/**
 * Récupérer le fichier envoyé
 * 
 * @return array|string
 *     string en cas d'erreur
 */
function formulaires_joindre_banniere_trouver_fichier_envoye() {
	$post = $_FILES ?? $GLOBALS['HTTP_POST_FILES'];

	if ( !is_array($post) || !isset($post['banniere_upload']) ) {
		return _T('medias:erreur_indiquez_un_fichier');
	}

	include_spip('inc/documents');

	// il y a une erreur
	if ( $post['banniere_upload']['error'] != 0 ) {
		$message_erreur = check_upload_error($post['banniere_upload']['error'], false, true);
		if ( $message_erreur && is_string($message_erreur) ) {
			return $message_erreur;
		}
		// check_upload_error() renvoit `true` quand `UPLOAD_ERR_NO_FILE`
		return _T('medias:erreur_indiquez_un_fichier');
	}

	// vérifier le format
	$extension = corriger_extension(strtolower(pathinfo($post['banniere_upload']['name'], PATHINFO_EXTENSION)));
	$formats_bannieres = charger_fonction('formats_bannieres', 'inc');
	if ( !array_key_exists($extension, $formats_bannieres()) ) {
		return _T('medias:erreur_format_fichier_image', ['nom' => $post['banniere_upload']['name']]);
	}

	return $post['banniere_upload'];
}