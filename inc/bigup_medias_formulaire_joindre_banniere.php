<?php
/**
 * Fonctions pour `bigup`
 *
 * @author		Jonathan OCHEJ
 * @license		GPL - https://www.gnu.org/licenses/gpl-3.0.html
 **/
if ( !defined('_ECRIRE_INC_VERSION') ) {
	return;
}

/**
 * @param array $args
 * @param \Spip\Bigup\Formulaire $formulaire
 * @return \Spip\Bigup\Formulaire
 */
function inc_bigup_medias_formulaire_joindre_banniere_dist($args, $formulaire) {

	$objet = isset($args['args'][0]) ? $args['args'][0] : 'site';
	$id_objet = isset($args['args'][1]) ? intval($args['args'][1]) : 0;

	$formulaire->preparer_input(
		'banniere_upload',
		[
			'previsualiser'			=> true,
			'input_class'			=> 'bigup_banniere',
			'drop-zone-extended'	=> '#formulaire_joindre_banniere_'.$objet.'_'.$id_objet,
		]
	);

	$formulaire->inserer_js('bigup_banniere.js');

	return $formulaire;
}