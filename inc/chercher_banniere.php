<?php
/**
 * Recherche de bannière
 *
 * @author		Jonathan OCHEJ
 * @license		GPL - https://www.gnu.org/licenses/gpl-3.0.html
 **/
if ( !defined('_ECRIRE_INC_VERSION') ) {
	return;
}

/**
 * Cherche la bannière d'un élément d'objet
 *
 * @param string $objet
 *     Objet SPIP auquel sera lié la bannière (ex. article)
 * @param int $id_objet
 *     Identifiant de l'objet
 * 
 * @return array
 *     Liste :
 *      - chemin complet du fichier
 *      - répertoire des bannières
 *      - nom de la bannière
 *      - extension de la bannière
 *      - date de modification
 *      - [doc]
 * 
 *     Tableau vide si aucune bannière trouvée
 */
function inc_chercher_banniere_dist($objet, $id_objet) {

	// chercher dans la base
	$doc = sql_fetsel('D.*', 'spip_documents AS D JOIN spip_documents_liens AS L ON L.id_document=D.id_document', 'D.mode=\'banniere\' AND L.objet=' . sql_quote($objet) . ' AND id_objet=' . intval($id_objet));
	if ( $doc ) {
		include_spip('inc/documents');
		$d = get_spip_doc($doc['fichier']);
		return [$d, _DIR_IMG, basename($d), $doc['extension'], @filemtime($d), $doc];
	}

	return [];
}