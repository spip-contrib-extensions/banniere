<?php
/**
 * Formats des bannières
 *
 * @author		Jonathan OCHEJ
 * @license		GPL - https://www.gnu.org/licenses/gpl-3.0.html
 **/
if ( !defined('_ECRIRE_INC_VERSION') ) {
	return;
}

/**
 * Cherche les formats supportés par les bannières
 * 
 * @return array
 *     Liste des formats supportés
 *     - cle : extension
 *     - valeur : mime_type
 * 
 *     Tableau vide si aucun format trouvé
 */
function inc_formats_bannieres_dist() {
	static $formats_bannieres = [];

	if ( !$formats_bannieres ) {
		$r = sql_select('extension, mime_type', 'spip_types_documents', "inclus='image' AND upload='oui'");
		while ( $e = sql_fetch($r) ) {
			$formats_bannieres[$e['extension']] = $e['mime_type'];
		}
	}

	return $formats_bannieres;
}