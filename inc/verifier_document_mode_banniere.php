<?php
/**
 * Vérification du téléversement de bannière par le plugin `medias`
 *
 * @author		Jonathan OCHEJ
 * @license		GPL - https://www.gnu.org/licenses/gpl-3.0.html
 **/
if ( !defined('_ECRIRE_INC_VERSION') ) {
	return;
}

/**
 * @param array $infos
 * 
 * @return bool|string
 *     string en cas d'erreur
 */
function inc_verifier_document_mode_banniere_dist($infos) {

	// si on veut téléverser une image, il faut qu'elle ait été bien lue
	if ( $infos['inclus'] != 'image' ) {
		return _T('medias:erreur_format_fichier_image', ['nom' => $infos['fichier']]);
	}

	if ( isset($infos['largeur']) && isset($infos['hauteur']) ) {
		if ( !($infos['largeur'] || $infos['hauteur']) ) {
			return _T('medias:erreur_upload_vignette', ['nom' => $infos['fichier']]);
		}
	}

	return true;
}