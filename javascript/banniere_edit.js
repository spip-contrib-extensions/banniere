/**
 * Surcharge du lien de modification d'une bannière pour ouvrir une `popup`
 *
 * @author		Jonathan OCHEJ
 * @license		GPL - https://www.gnu.org/licenses/gpl-3.0.html
 * 
 **/
(function(){
	function bannieres_editbox_init() {
		jQuery(':where(.formulaire_joindre_banniere) a.editbox:not(.nobox)')
		.attr("onclick","").addClass('nobox').click(function(){
			var apercu = jQuery(this).parents('.apercu_banniere').eq(0);
			jQuery(apercu).animateLoading();
			jQuery.modalboxload(parametre_url(parametre_url(jQuery(this).attr('href'),'popin','oui'),'var_zajax','contenu'),{
				onClose: function (dialog) {jQuery(apercu).ajaxReload();}
			});
			return false;
		});
	}
	if ( window.jQuery ) {
		(function($){
			if(typeof onAjaxLoad == "function") {
				onAjaxLoad(bannieres_editbox_init);
			}
			$(bannieres_editbox_init);
		})(jQuery);
	}
})();