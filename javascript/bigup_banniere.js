/**
 * Gérer le formulaire des bannieres avec `bigup`
 *
 * @author		Jonathan OCHEJ
 * @license		GPL - https://www.gnu.org/licenses/gpl-3.0.html
 * 
 **/
function formulaires_banniere_avec_bigup() {
	// trouver les input qui envoient des fichiers
	$(".formulaire_joindre_banniere form")
		.find(".joindre_banniere")
		.find("label").hide().end()
		.find("input[type=file].bigup_banniere")
		.not('.bigup_done')
		.bigup()
		.on('bigup.fileSuccess', function(event, file, description) {
			const bigup = file.bigup
			const input = file.emplacement

			const data = bigup.buildFormData()
			data.set('formulaire_action_verifier_json', true)
			data.set('bigup_reinjecter_uniquement', [description.bigup.identifiant])

			// verifier les champs
			bigup
			.send(data, {dataType: 'json'})
			.done(function(erreurs) {
				var erreur = erreurs[bigup.name] || erreurs.message_erreur;
				if (erreur) {
					bigup.presenter_erreur(input, erreur);
				} else {
					data.delete('formulaire_action_verifier_json');
					var conteneur = bigup.form.parents('.formulaire_joindre_banniere');
					conteneur.animateLoading();
					// Faire le traitement prévu, supposant qu'il n'y aura pas d'erreur...
					bigup
					.send(data)
					.done(function(html) {
						bigup.presenter_succes(input, _T('banniere:la_banniere_a_ete_envoyee'));
						bigup.form.parents('.formulaire_spip').parent().html(html);
					})
					.fail(function(data) {
						conteneur.endLoading();
						bigup.presenter_erreur(input, _T('bigup:erreur_probleme_survenu'));
					});
				}
			})
			.fail(function(data) {
				bigup.presenter_erreur(input, _T('bigup:erreur_probleme_survenu'));
			});

		})
		.closest('.editer').find('.dropfiletext').html(_T('banniere:deposer_la_banniere_ici'))
}

jQuery(function($) {
	formulaires_banniere_avec_bigup()
	onAjaxLoad(formulaires_banniere_avec_bigup)
})