/**
 * Gérer le cadrage des bannieres avec `centre_image`
 *
 * @author		Jonathan OCHEJ
 * @license		GPL - https://www.gnu.org/licenses/gpl-3.0.html
 * 
 **/
$(document).on('touchmove MSPointerMove pointermove mousemove', function() {
	if ( t_centre_actif ) {
		let lien = t_centre_actif.parent('a')
		if ( lien.hasClass('lien_apercu_banniere') ) {
			let cadre = lien.find('.cadre_apercu_banniere')
			if ( cadre ) {
				let padding = 2 // outline + border du cadre
				let hauteur_cadre = cadre.height()

				let position_centre_actif_y = parseInt(t_centre_actif.css('top'))

				// atention, pas de demi-pixel pour le calcul
				let position_y_min = Math.ceil(hauteur_cadre / 2) + padding
				let position_y_max = centre_image_y_max - (Math.ceil(hauteur_cadre / 2) + padding)

				if ( position_centre_actif_y < position_y_min ) {
					t_centre_actif.css('top', position_y_min + 'px')
					position_centre_actif_y = position_y_min

				}
				else if ( position_centre_actif_y > position_y_max ) {
					t_centre_actif.css('top', position_y_max + 'px')
					position_centre_actif_y = position_y_max
				}
				
				let y = position_centre_actif_y / centre_image_y_max
				y = Math.max(0, y)
				y = Math.min(1, y)

				// changer la position vertical de l'aperçu
				lien.css('--centre-image-y', y)
			}
		}
	}
})